### For

Go has only one looping construct, the for loop.

The basic for loop has three components separated by semicolons:

* the init statement: executed before the first iteration
* the condition expression: evaluated before every iteration
* the post statement: executed at the end of every iteration

```go
package main

import "fmt"

func main(){
	for i := 1; i < 10; i++ {
		fmt.Println(i)
	}
}
```

The init statement will often be a short variable declaration, and the variables declared there are visible only in the scope of the for statement.

The init and post statements are optional.

```go
for ; i < 10; {
	fmt.Println(i)
}
```

### For is Go's "while"

In Go's while loop at that time you can drop the semicolons

```go 
fun main(){
	i := 1
	for i < 10 {
		i++
	}
	fmt.Println(i)
}
```

Infinit loop like

```go
for {
	...
}
```
