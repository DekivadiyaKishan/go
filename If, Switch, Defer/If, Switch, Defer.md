### If

If statment is like for loop not need to surrounded by parentheses **()** but the braces  **{}** are required.

```go 
func main(){
	a := 4
	b := 5
	if a < b {
		fmt.Println("B is Greater then A")
	}
}
```

Like for, the if statement can start with a short statement to execute before the condition.Variables declared by the statement are only in scope until the end of the if.

```go
func pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		return v
	}
	return lim
}
```

Variables declared inside an if short statement are also available inside any of the else blocks.

```go
func pow(x, n, lim float64) float64 {
	if v := math.Pow(x, n); v < lim {
		return v
	}else {
		return lim
	}
}
```

### Switch

Switch statment is a shorter way to write a sequence of if-else statments.Go not need of **break** statment beacuse of Go only runs the selected case, not all the cases that follow.

```go
switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		fmt.Printf("%s.\n", os)
	}
```

### Defer

A defer statement defers the execution of a function until the surrounding function returns.

The deferred call's arguments are evaluated immediately, but the function call is not executed until the surrounding function returns.

```go 
package main

import "fmt"

func main() {
	defer fmt.Println("world")

	fmt.Println("hello")
}
```
