# Functions

Function can take zero or more arguments.

```go
func add(x int, y int) int {
	return x + y
}
```

Notice that type comes after the variable name.

check [Go Declaration syntax.](https://blog.golang.org/gos-declaration-syntax)

In function consecutive named funcation parameters share a type, you can omit type from all but the last.

```go
x int, y int
```

to

```go
x, y int
```

A function can return any number of results.

```go
func swap(x, y string) (string, string) {
	return y, x
}
```

also write returns like above function

```go
func swap(x, y string) (y, x string) {
	return y, x
}
```

above function is anotherway to return function value. This method is known as a **naked** return.
