package main

import "fmt"

func add(x, y int) int {
	return x + y
}

func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

func swap(x, y string) (string, string) {
	return y, x
}

func main(){
	fmt.Println(add(10,12))
	fmt.Println(split(17))
	fmt.Println(swap(25,50))
}
