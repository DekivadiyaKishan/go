# Variables

The **var** statement declares a list of variables; as in function argument lists, the type is last.

A **var** statement can be at package or function level. We see both in this example.

```go
package main

import "fmt"

var greetings string

func main() {
	greetings = "Good Morning"
	fmt.Println(greetings)
}
```

A var declaration can include initializers, one per variable.If an initializer is present, the type can be omitted; the variable will take the type of the initializer.

```go
var i, j int = 1, 2
var a, b = false, "Hello"
```

Inside a function, the := short assignment statement can be used in place of a var declaration with implicit type.Outside a function, every statement begins with a keyword (var, func, and so on) and so the := construct is not available. This is called short variable declarations

```go
func main(){
	a := 4
	greeting, check := "Good Morning", false
}
```

Go's basic types are

```go
bool

string

int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64 uintptr

byte // alias for uint8

rune // alias for int32
     // represents a Unicode code point

float32 float64

complex64 complex128
```

Variables declared without an explicit initial value are given their zero value.

##### Type conversions

```go
var i int = 42
var f float64 = float64(i)
var u uint = uint(f)
```

or

```go
i := 42
f := float64(i)
u := uint(f)
```
Constants are declared like variables, but with the const keyword.Constants can be character, string, boolean, or numeric values.Constants cannot be declared using the := syntax.

```go
const Pi = 3.14	
```

