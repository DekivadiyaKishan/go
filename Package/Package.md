# Packages

Go program is made up of packages. In Go every programs start running in main package.

```go
package main

import (
	"fmt"
	"math/rand"
)

func main() {
	fmt.Println("My favorite number is", rand.Intn(10))
}
```
Here, We are use **fmt** and **math/rand**.

**math/rand** package comprises files that begin with the statment package rand.

# Imports

In other languages we are used import statments, like:

```go
import "fmt"
import "math"
```

but, In Go also using like:

```go
import(
	"fmt"
	"math"
)
```

this method calles as a **factored** import statment.
