# Go

This Project for complete Go language. Here you can get all instruction about Go language.

# System requirements

| Operating System | Architechtures | Notes |
| ------------- |:-------------:| ----- |
| FreeBSD 10.3 or later | amd64, 386 | Debian GNU/kFreeBSD not supported |
| Linux 2.6.23 or later with glibc | amd64, 386, arm, arm64, s390x, ppc64le | CentOS/RHEL 5.x not suppoerted. Install from source for other libc. |
| macOS 10.10 or later | amd64 | use the clang or gcc that comes with Xcode for cgo support |
| Window 7, server 2008R2 or later | amd64, 386 | use MinGW(386) or MinGW-W64(amd64) gcc. No need for cygwin or msys.
  
# Install the go tools

### Linux, macOS, and FreeBSD tarballs
	
[Download the archive](https://golang.org/dl/) and extract it into /usr/local, creating a Go tree in /usr/local/go.

> tar -C /usr/local -xzf go$VERSION.$OS-$ARCH.tar.gz
	
Add /usr/local/go/bin to the PATH environment variable. You can do this by adding this line to your /etc/profile (for a system-wide installation) or 	$HOME/.profile:
	
> export PATH=$PATH:/usr/local/go/bin
	
**Note**: changes made to a profile file may not apply until the next time you log into your computer. To apply the changes immediately, just run the 	shell 	commands directly or execute them from the profile using a command such as source $HOME/.profile.

### macOS package installer
	
[Download the package](https://golang.org/dl/) file, open it, and follow the prompts to install the Go tools. The package installs the Go distribution to /usr/local/go.

The package should put the /usr/local/go/bin directory in your PATH environment variable. You may need to restart any open Terminal sessions for the 	change to take effect.

### Windows
	
The Go project provides two installation options for Windows users (besides installing from source): a zip archive that requires you to set some environment variables and an MSI installer that configures your installation automatically.

#### MSI installer
	
Open the [MSI file](https://golang.org/dl/) and follow the prompts to install the Go tools. By default, the installer puts the Go distribution in c:\Go.

The installer should put the c:\Go\bin directory in your PATH environment variable. You may need to restart any open command prompts for the change to take effect.

#### Zip archive
	
[Download the zip](https://golang.org/dl/) file and extract it into the directory of your choice (we suggest c:\Go).

Add the bin subdirectory of your Go root (for example, c:\Go\bin) to your PATH environment variable.

#### Setting environment variables under Windows
	
Under Windows, you may set environment variables through the "Environment Variables" button on the "Advanced" tab of the "System" control panel. Some versions of Windows provide this control panel through the "Advanced System Settings" option inside the "System" control panel.
