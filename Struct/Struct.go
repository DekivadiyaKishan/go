package main

import (
	"fmt"
	"strings"
)

type Vertex struct {
	X int
	Y int
}

func main() {
	//Declaration of Struct
	x := Vertex{}
	x.X = 4
	fmt.Println(x.X)
	p := &x // store address of a vertex variable
	fmt.Println(p.Y)
	p.Y = 6
	fmt.Println("Print using Pointer :", p.Y)
	fmt.Println("Print direct:", x.Y)
}
