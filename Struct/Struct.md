# Structs

A struct is a collection of fields. Struct define like

```go
type Vertex struct {
	X int
	Y int
}
```

Using dot we can access struct fields.

```go
func main() {
	v := Vertex{1, 2}
	v.X = 4
	fmt.Println(v.X)
}
```
