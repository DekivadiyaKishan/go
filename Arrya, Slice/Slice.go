package main

import "fmt"

func main() {
	primes := [6]int{2, 3, 5, 7, 11, 13}
	fmt.Println(primes)

	//slice array and store in int array
	var s []int = primes[1:4]
	fmt.Println(s)
	
	//Slices are like references to arrays
	names := [4]string{
		"Kishan",
		"Harsh",
		"Rooshita",
		"Jenisha",
	}
	fmt.Println(names)

	a := names[0:2]
	b := names[1:3]
	fmt.Println(a, b)

	b[0] = "Mansi"
	fmt.Println(a, b)
	fmt.Println(names)
	
	s := []int{2, 3, 5, 7, 11, 13}
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)

	// Slice the slice to give it zero length.
	s = s[:0]
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)

	// Extend its length.
	s = s[:4]
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)

	// Drop its first two values.
	s = s[2:]
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

