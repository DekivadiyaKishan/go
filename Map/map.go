package main

import (
	"fmt"
)

func main(){
	var m map[string]int
	
	fmt.Println(m)

	sem := make(map[string]int)

	sem["kishan"] = 8
	sem["harsh"] = 8
	sem["jenish"] = 4
	fmt.Println(sem["kishan"])
	fmt.Println(sem["rushi"])

	sem2 := map[string]int{
		"kishan" : 8,
		"harsh" : 8,
		"rooshita" : 4,
	}
	fmt.Println(sem2["rooshita"])
	
	kishanSem, kishanOk := sem["kishan"]
	fmt.Println(kishanSem, kishanOk)

	fmt.Println("How many in map",len(sem2))
	
	fmt.Println("Before delete ",sem2["kishan"])
	delete(sem2, "kishan")
	fmt.Println("After delete ",sem2["kishan"])

	for key, value := range(sem2){
		fmt.Println(key, "=>", value)
	}
}
